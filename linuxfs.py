"""This module implements a pure-Python wrapper around various
Linux-specific filesystem functions."""
#+
# Copyright 2022-2024 Lawrence D'Oliveiro <ldo@geek-central.gen.nz>.
# Licensed under the GNU Lesser General Public License v2.1 or later.
#-

import os
import errno
import enum
from functools import \
    reduce
import operator
import ctypes as ct
import atexit

#+
# Useful stuff
#-

def make_funcptr(lib, name) :
    "returns a new, unique ctypes object representing the same" \
    " entry point in lib named name. This can have its own argtypes" \
    " and restype definitions, distinct from any other object representing" \
    " the same entry point."
    ep = getattr(lib, name)
    return \
        type(ep).from_address(ct.addressof(ep))
#end make_funcptr

class _BIT_FLAG(enum.Enum) :
    "subclasses have instances that are bit numbers or tuples" \
    " of them, but have a mask property which is the corresponding" \
    " bit mask."

    @property
    def mask(self) :
        "convert bit number(s) to mask."
        return \
            (
                lambda : 1 << self.value,
                lambda : reduce(operator.or_, (1 << v for v in self.value), 0),
            )[isinstance(self.value, tuple)]()
    #end mask

    @classmethod
    def encode_mask(celf, bits) :
        "accepts an event mask as either an integer or a set of enum values," \
        " and always returns the integer form."
        if isinstance(bits, (set, frozenset)) and all(isinstance(i, celf) for i in bits) :
            result = 0
            for bit in bits :
                result |= bit.mask
            #end for
        elif isinstance(bits, celf) :
            result = bits.mask
        elif isinstance(bits, int) :
            result = bits
        else :
            raise TypeError("bits must be integer mask, or set of %s flags" % celf.__name__)
        #end if
        return result
    #end encode_mask

    @classmethod
    def make_set(celf, mask : int) :
        members = sorted \
          (
            celf.__members__.values(),
            key = lambda m : (lambda : 1, lambda : len(m.value))[isinstance(m.value, tuple)](),
            reverse = True
          )
        if members[-1].value == () and mask == 0 :
            result = {members[-1]}
        else :
            if members[-1].value == () :
                members = members[:-1]
            #end if
            remaining = mask
            removed = 0
            result = set()
            for m in members :
                if m.mask & remaining != 0 and m.mask & ~removed & remaining == m.mask & ~removed :
                    result.add(m)
                    remaining &= ~m.mask
                    removed |= m.mask
                #end if
            #end for
            if remaining != 0 :
                raise ValueError("undefined bits remaining in mask: %#08x" % remaining)
            #end if
        #end if
        return result
    #end make_set

#end _BIT_FLAG

#+
# Low-level definitions
#-

# from /usr/include/«arch»/bits/fcntl-linux.h:
class file_handle(ct.Structure) :
    _fields_ = \
        [
            ("handle_bytes", ct.c_uint),
            ("handle_type", ct.c_int),
            ("f_handle", 0 * ct.c_ubyte), # actual size is handle_bytes
        ]
#end file_handle

libc = ct.CDLL("libc.so.6", use_errno = True)

libc.pidfd_open.restype = ct.c_int
libc.pidfd_open.argtypes = (ct.c_int, ct.c_uint)
# pidfd_getfd, pidfd_send_signal not needed (yet)
libc.name_to_handle_at.restype = ct.c_int
libc.name_to_handle_at.argtypes = (ct.c_int, ct.c_char_p, ct.POINTER(file_handle), ct.POINTER(ct.c_int), ct.c_int)
libc.open_by_handle_at.restype = ct.c_int
libc.open_by_handle_at.argtypes = (ct.c_int, ct.POINTER(file_handle), ct.c_int)

# following comes from /usr/include/bits/fcntl-linux.h or /usr/include/linux/fcntl.h
AT_FDCWD = -100 # special fd value indicating current working directory
AT_NO_AUTOMOUNT = 0x800
AT_EMPTY_PATH = 0x1000 # needs CAP_DAC_READ_SEARCH privilege to use
AT_RECURSIVE = 0x8000
AT_SYMLINK_FOLLOW = 0x400 # no special privileges necessary

class SYS :
    "syscall codes."
    # actual numeric syscall codes (__NR_xxx) can be found in /usr/include/asm-generic/unistd.h.
    # /usr/include/bits/syscall.h just defines SYS_xxx synonyms for these.
    linkat = 37
    # pidfd_open = 434
      # not needed? Contrary to current man pages, the pidfd_xxx calls are in glibc.
    openat2 = 437
#end SYS

def def_syscall(name, code, args, res) :
    "creates a function which invokes syscall(2) with the given code and taking" \
    " additional arguments with the given types, returning a result of the given" \
    " type."
    func = make_funcptr(libc, "syscall")
    func.argtypes = (ct.c_long,) + args
    func.restype = res

    def callit(*args) :
        return \
            func(code, *args)
    #end callit

#begin def_syscall
    callit.__name__ = name
    return \
        callit
#end def_syscall

class OPENAT2 :
    "definitions from /usr/include/linux/openat2.h."

    class open_how(ct.Structure) :
        _fields_ = \
            [
                ("flags", ct.c_uint64),
                ("mode", ct.c_uint64),
                ("resolve", ct.c_uint64),
            ]
    #end open_how

    # mask bits for open_how.resolve
    RESOLVE_NO_XDEV = 0x01 # no crossing mount points
    RESOLVE_NO_MAGICLINKS = 0x02 # no following “magic symlinks”
    RESOLVE_NO_SYMLINKS = 0x04 # no following any symlinks, magic or otherwise
    RESOLVE_BENEATH = 0x08 # no going above hierarchy of dirfd
    RESOLVE_IN_ROOT = 0x10 # interpret “/” and “..” as staying within dirfd, as though chroot were in effect
    RESOLVE_CACHED = 0x20 # only do cached lookups, may return -EAGAIN

#end OPENAT2

class RESOLVE(_BIT_FLAG) :

    NO_XDEV = 0
    NO_MAGICLINKS = 1
    NO_SYMLINKS = 2
    BENEATH = 3
    IN_ROOT = 4
    CACHED = 5

#end RESOLVE

libc.ioctl.argtypes = (ct.c_int, ct.c_ulong, ct.c_void_p)
libc.ioctl.restype = ct.c_int
libc.linkat.argtypes = (ct.c_int, ct.c_char_p, ct.c_int, ct.c_char_p, ct.c_int)
libc.linkat.restype = ct.c_int
libc.renameat2.argtypes = (ct.c_int, ct.c_char_p, ct.c_int, ct.c_char_p, ct.c_uint)
libc.renameat2.restype = ct.c_int

# from </usr/include/linux/fs.h>:
RENAME_NOREPLACE = 1 << 0 # fail if newpath already exists
RENAME_EXCHANGE = 1 << 1 # exchange names
RENAME_WHITEOUT = 1 << 2 # create whiteout object
  # (overlay/unionfs-type systems only) (requires CAP_MKNOD)

openat2 = def_syscall \
  (
    "openat2",
    SYS.openat2,
    (ct.c_int, ct.c_char_p, ct.POINTER(OPENAT2.open_how), ct.c_size_t),
    ct.c_long
  )

class _IOC :
    # from </usr/include/asm-generic/ioctl.h>
    NRBITS = 8
    TYPEBITS = 8

    SIZEBITS = 14 # note -- architecture-specific!
    DIRBITS = 2 # note -- architecture-specific!

    NRMASK = (1 << NRBITS) - 1
    TYPEMASK = (1 << TYPEBITS) - 1
    SIZEMASK = (1 << SIZEBITS) - 1
    DIRMASK = (1 << DIRBITS) - 1

    NRSHIFT = 0
    TYPESHIFT = NRSHIFT + NRBITS
    SIZESHIFT = TYPESHIFT + TYPEBITS
    DIRSHIFT = SIZESHIFT + SIZEBITS

    NONE = 0 # note -- architecture-specific!
    WRITE = 1 # note -- architecture-specific!
    READ = 2 # note -- architecture-specific!
    TYPECHECK = lambda t : \
        (
            (
                lambda : ct.sizeof(t),
                lambda : t,
            )[isinstance(t, int)]()
        )

    # for decoding codes constructed by _IOC() and derivatives:
    DIR = lambda nr : nr >> DIRSHIFT & DIRMASK
    TYPE = lambda nr : nr >> TYPESHIFT & TYPEMASK
    NR = lambda nr : nr >> NRSHIFT & NRMASK
    SIZE = lambda nr : nr >> SIZESHIFT & SIZEMASK

#end _IOC
_IOC.IOC = lambda dir, type, nr, size : \
    (
        dir << _IOC.DIRSHIFT
    |
            (
                lambda : type,
                lambda : ord(type),
            )[isinstance(type, str)]()
        <<
            _IOC.TYPESHIFT
    |
        nr << _IOC.NRSHIFT
    |
        size << _IOC.SIZESHIFT
    )
# convenience wrappers around IOC():
_IOC.IO = lambda type, nr : _IOC.IOC(_IOC.NONE, type, nr, 0)
_IOC.IOR = lambda type, nr, size : _IOC.IOC(_IOC.READ, type, nr, _IOC.TYPECHECK(size))
_IOC.IOW = lambda type, nr, size : _IOC.IOC(_IOC.WRITE, type, nr, _IOC.TYPECHECK(size))
_IOC.IOWR = lambda type, nr, size : _IOC.IOC(_IOC.READ | _IOC.WRITE, type, nr, _IOC.TYPECHECK(size))
_IOC.IOR_BAD = lambda type, nr, size : _IOC.IOC(_IOC.READ, type, nr, ct.sizeof(size))
_IOC.IOW_BAD = lambda type, nr, size : _IOC.IOC(_IOC.WRITE, type, nr, ct.sizeof(size))
_IOC.IOWR_BAD = lambda type, nr, size : _IOC.IOC(_IOC.READ | _IOC.WRITE, type, nr, sizeof(size))

class FS :
    "definitions of codes and flag bits that you will need."

    # from </usr/include/linux/fs.h>
    # Note this is not the same thing as “extended attributes” (xattr(7)),
    # for which standard Python library calls already exist.
    class xattr(ct.Structure) :
        _fields_ = \
            [
                ("fsx_xflags", ct.c_uint32),
                ("fsx_extsize", ct.c_uint32),
                ("fsx_nextents", ct.c_uint32),
                ("fsx_projid", ct.c_uint32),
                ("fsx_cowextsize", ct.c_uint32),
                ("fsx_pad", 8 * ct.c_ubyte),
            ]

        def copy(self) :
            celf = type(self)
            res = celf()
            for f in celf._fields_ :
                setattr(res, f[0], getattr(self, f[0]))
            #end for
            return \
                res
        #end copy

        def __repr__(self) :
            return \
                (
                    "(%s)"
                %
                    ", ".join
                      (
                            "%%s = %s" % ("%d", "%#0.8x")[f == "fsx_xflags"]
                        %
                            (f, getattr(self, f))
                        for f in
                            ("fsx_xflags", "fsx_extsize", "fsx_nextents", "fsx_projid", "fsx_cowextsize")
                      )
                )
        #end __repr__

    #end xattr

    XFLAG_REALTIME = 0x00000001
    XFLAG_PREALLOC = 0x00000002
    XFLAG_IMMUTABLE = 0x00000008
    XFLAG_APPEND = 0x00000010
    XFLAG_SYNC = 0x00000020
    XFLAG_NOATIME = 0x00000040
    XFLAG_NODUMP = 0x00000080
    XFLAG_RTINHERIT = 0x00000100
    XFLAG_PROJINHERIT = 0x00000200
    XFLAG_NOSYMLINKS = 0x00000400
    XFLAG_EXTSIZE = 0x00000800
    XFLAG_EXTSZINHERIT = 0x00001000
    XFLAG_NODEFRAG = 0x00002000
    XFLAG_FILESTREAM = 0x00004000
    XFLAG_DAX = 0x00008000
    XFLAG_COWEXTSIZE = 0x00010000
    XFLAG_HASATTR = 0x80000000

    FSLABEL_MAX = 256

    IOC_GETFLAGS = _IOC.IOR('f', 1, ct.c_long)
    IOC_SETFLAGS = _IOC.IOW('f', 2, ct.c_long)
    IOC_GETVERSION = _IOC.IOR('v', 1, ct.c_long)
    IOC_SETVERSION = _IOC.IOW('v', 2, ct.c_long)
    # IOC_FIEMAP = _IOC.IOWR('f', 11, struct fiemap) # from </usr/include/linux/fiemap.h>
    IOC32_GETFLAGS = _IOC.IOR('f', 1, ct.c_int)
    IOC32_SETFLAGS = _IOC.IOW('f', 2, ct.c_int)
    IOC32_GETVERSION = _IOC.IOR('v', 1, ct.c_int)
    IOC32_SETVERSION = _IOC.IOW('v', 2, ct.c_int)
    IOC_FSGETXATTR = _IOC.IOR('X', 31, xattr)
    IOC_FSSETXATTR = _IOC.IOW('X', 32, xattr)
    IOC_GETFSLABEL = _IOC.IOR(0x94, 49, FSLABEL_MAX)
    IOC_SETFSLABEL = _IOC.IOW(0x94, 50, FSLABEL_MAX)

    # see ioctl_iflags(2) man page for info about these
    SECRM_FL = 0x00000001
    UNRM_FL = 0x00000002
    COMPR_FL = 0x00000004
    SYNC_FL = 0x00000008
    IMMUTABLE_FL = 0x00000010
    APPEND_FL = 0x00000020
    NODUMP_FL = 0x00000040
    NOATIME_FL = 0x00000080
    DIRTY_FL = 0x0000010
    COMPRBLK_FL = 0x00000200
    NOCOMP_FL = 0x00000400
    ENCRYPT_FL = 0x00000800
    BTREE_FL = 0x00001000
    INDEX_FL = 0x00001000
    IMAGIC_FL = 0x00002000
    JOURNAL_DATA_FL = 0x00004000
    NOTAIL_FL = 0x00008000
    DIRSYNC_FL = 0x00010000
    TOPDIR_FL = 0x00020000
    HUGE_FILE_FL = 0x00040000
    EXTENT_FL = 0x00080000
    VERITY_FL = 0x00100000
    EA_INODE_FL = 0x00200000
    EOFBLOCKS_FL = 0x00400000
    NOCOW_FL = 0x00800000
    DAX_FL = 0x02000000
    INLINE_DATA_FL = 0x10000000
    PROJINHERIT_FL = 0x20000000
    CASEFOLD_FL = 0x40000000
    RESERVED_FL = 0x80000000

    FL_USER_VISIBLE = 0x0003DFFF
    FL_USER_MODIFIABLE = 0x000380FF

#end FS

class PIDFD :
    # from </usr/include/linux/pidfd.h>
    NONBLOCK = os.O_NONBLOCK
#end PIDFD

#+
# Higher-level stuff
#-

def _get_fileno(fd, argname = "fd") :
    # common code to allow caller to pass either an integer file
    # descriptor or an object with the usual Python fileno() method
    # that returns such a file descriptor.
    if not isinstance(fd, int) :
        if hasattr(fd, "fileno") :
            fd = fd.fileno()
        else :
            raise TypeError("%s arg must be int fileno or object with fileno() method" % argname)
        #end if
    #end if
    return \
        fd
#end _get_fileno

def _get_path(pathname, argname) :
    # returns a utf-8-encoded representation of pathname.
    if isinstance(pathname, str) :
        c_pathname = pathname.encode()
    elif not isinstance(pathname, (bytes, bytearray)) :
        raise TypeError("%s must be string or bytes" % argname)
    else :
        c_pathname = pathname
    #end if
    return \
        c_pathname
#end _get_path

def _check_sts(sts) :
    if sts < 0 :
        errn = ct.get_errno()
        raise OSError(errn, os.strerror(errn))
    #end if
#end _check_sts

def getflags(fd) :
    flags = ct.c_long()
    _check_sts(libc.ioctl(_get_fileno(fd), FS.IOC_GETFLAGS, ct.byref(flags)))
    return \
        flags.value
#end getflags

def setflags(fd, flags) :
    c_flags = ct.c_long(flags)
    _check_sts(libc.ioctl(_get_fileno(fd), FS.IOC_SETFLAGS, ct.byref(c_flags)))
#end setflags

def getfsxattr(fd) :
    "returns the fsx_ attribute flags as found in </usr/include/linux/fs.h>."
    xattr = FS.xattr()
    _check_sts(libc.ioctl(_get_fileno(fd), FS.IOC_FSGETXATTR, ct.byref(xattr)))
    return \
        xattr
#end getfsxattr

def setfsxattr(fd, *args, **kwargs) :
    "sets new values for the fsx_ attribute flags as found in" \
    " </usr/include/linux/fs.h>. Call this as follows:\n" \
    "\n" \
    "    setfsxattr(fd[, «newattrs»], [«field» = «value», ...])\n" \
    "\n" \
    "where «newattrs» is a FS.xattr struct, and the individual «fields» of" \
    " the FS.xattr struct can also be specified by name. Any fields not" \
    " specified by name keep their value from «newattrs», or are set to" \
    " zero if «newattrs» is not specified. This allows you to selectively" \
    " change certain fields with a call like\n" \
    "\n" \
    "    setfsxattr(fd, getfsxattr(fd), «field» = «value», ...)"
    if len(args) + len(kwargs) == 0 :
        raise TypeError("nothing to do")
    #end if
    if len(args) > 1 :
        raise TypeError("only expecting one positional argument")
    #end if
    if len(args) > 0 :
        xattr = args[0]
        if not isinstance(xattr, FS.xattr) :
            raise TypeError("positional argument is not a FS.xattr struct")
        #end if
    else :
        xattr = FS.xattr()
    #end if
    if len(kwargs) > 0 :
        valid = set(f[0] for f in FS.xattr._fields_ if f[0] != "fsx_pad")
        for field in kwargs :
            if field not in valid :
                raise TypeError("invalid FS.xattr keyword %s" % field)
            #end if
            setattr(xattr, field, kwargs[field])
        #end for
    #end if
    _check_sts(libc.ioctl(_get_fileno(fd), FS.IOC_FSSETXATTR, ct.byref(xattr)))
#end setfsxattr

def open_at(dirfd, pathname, **kwargs) :
    "convenient wrapper around openat2(2) which breaks out fields of open_how" \
    " struct into separate keyword args (flags, mode, resolve). Returns open" \
    " file descriptor on success."
    how = OPENAT2.open_how()
    valid = set(f[0] for f in OPENAT2.open_how._fields_)
    for field in kwargs :
        if field not in valid :
            raise TypeError("invalid OPENAT2 keyword %s" % field)
        #end if
        val = kwargs[field]
        if field == "resolve" :
            val = RESOLVE.encode_mask(val)
        #end if
        setattr(how, field, val)
    #end for
    res = openat2 \
      (
        _get_fileno(dirfd, "dirfd"),
        _get_path(pathname, "pathname"),
        ct.byref(how),
        ct.sizeof(how)
      )
    _check_sts(res)
    return \
        res
#end open_at

def get_magic_symlink(fd) :
    "returns the “magic symlink” for a currently-open file."
    return \
        "/proc/self/fd/%d" % _get_fileno(fd)
#end get_magic_symlink

def save_tmpfile(fd, path) :
    "assumes fd was previously created as an anonymous file with O_TMPFILE flag;" \
    " gives it the explicit name path, which must be on the same filesystem" \
    " where it was originally created. This is done following the procedure given" \
    " on the openat(2) man page."
    c_path = _get_path(path, "path")
    tmpfile_path = get_magic_symlink(fd)
    _check_sts(libc.linkat(AT_FDCWD, tmpfile_path.encode(), AT_FDCWD, c_path, AT_SYMLINK_FOLLOW))
#end save_tmpfile

def rename_at(olddirfd, oldpath, newdirfd, newpath, flags = 0) :
    "does a renameat2(2) call."
    res = libc.renameat2 \
      (
        _get_fileno(olddirfd, "olddirfd"),
        _get_path(oldpath, "oldpath"),
        _get_fileno(newdirfd, "newdirfd"),
        _get_path(newpath, "newpath"),
        flags
      )
    _check_sts(res)
#end rename_at

class FileHandle :
    "higher-level wrapper around a file_handle struct."

    __slots__ = ("hdlptr", "hdlbuf")

    @classmethod
    def frombuf(celf, hdlbuf) :
        "constructs a wrapper from an existing bytes/bytearray object."
        self = celf()
        self.hdlptr = ct.pointer(file_handle.from_buffer(hdlbuf))
        self.hdlbuf = hdlbuf
        return \
            self
    #def frombuf

    @classmethod
    def wrap(celf, hdlptr, hdlbuf) :
        "constructs a wrapper from a given file_handle pointer and associated" \
        " bytes/bytearray storage."
        self = celf()
        self.hdlptr = hdlptr
        self.hdlbuf = hdlbuf
        return \
            self
    #end wrap

    @classmethod
    def alloc(celf, handle_bytes : int) :
        "allocates storage for a handle with handle_bytes of dynamic data," \
        " and returns a wrapper FileHandle object for it."
        hdlbuf = bytearray((ct.sizeof(file_handle) + handle_bytes) * (0,))
        hdlptr = ct.pointer(file_handle.from_buffer(hdlbuf))
        hdlptr.contents.handle_bytes = handle_bytes
        return \
            celf.wrap(hdlptr, hdlbuf)
    #end alloc

    @property
    def handle_bytes(self) :
        return \
            self.hdlptr.contents.handle_bytes
    #end handle_bytes

#end FileHandle

def name_to_handle_at(dirfd, pathname, flags : int) :
    "returns a (handle, mount_id) tuple on success."
    c_dirfd = _get_fileno(dirfd, "dirfd")
    c_pathname = _get_path(pathname, "pathname")
    c_mountid = ct.c_int()
    hdl = FileHandle.alloc(0) # for initial call to get actual needed nr bytes
    first = True
    while True :
        res = libc.name_to_handle_at(c_dirfd, c_pathname, hdl.hdlptr, ct.byref(c_mountid), flags)
        if res == 0 :
            break
        errn = ct.get_errno()
        if errn != errno.EOVERFLOW or not first :
            raise OSError(errn, os.strerror(errn))
        #end if
        first = False
        ct.set_errno(0)
        hdl = FileHandle.alloc(hdl.handle_bytes)
    #end while
    return \
        (hdl, c_mountid.value)
#end name_to_handle_at

def open_by_handle_at(mount_fd, handle : FileHandle, flags : int) :
    c_mount_fd = _get_fileno(mount_fd, "mount_fd")
    if not isinstance(handle, FileHandle) :
        raise TypeError("handle must be a FileHandle")
    #end if
    res = libc.open_by_handle_at(c_mount_fd, handle.hdlptr, flags)
    _check_sts(res)
    return \
        res
#end open_by_handle_at

class TentativeFile :
    "Convenience class for implementing a safe-overwrite-in-place mechanism." \
    " Create one of these specifying the final pathname of the output file," \
    " which might or might not already exist. Use the open() method to create" \
    " a regular Python file object for writing to the new file. If you want to" \
    " read from the existing file with that name, you may open it before or after" \
    " creating this TentativeFile object; the existing file remains untouched" \
    " and accessible under its current name unless and until this new file is" \
    " explicitly saved.\n" \
    "\n" \
    "After finishing writing and closing the new file’s contents, call this class’s" \
    " save() method to actually make the new file appear in the filesystem under" \
    " the specified name, replacing any existing file of that name.\n" \
    "\n" \
    "The saving technique requires the new file to initially appear in the" \
    " filesystem under a temporary name, before assuming the specified name." \
    " This temporary name is formed by adding a suffix to the specified name," \
    " which can fail if there is already a file with that temporary name." \
    " To deal with this, it is possible to specify a number of additional retries," \
    " appending an additional incrementing numeric suffix each time, to find an" \
    " unused name for temporary use."

    __slots__ = ("fd", "dirfd", "pathname", "tempsuffix", "tempretry")

    def __init__(self, *, dirfd = None, pathname, readable = True, mode = 0o777, tempsuffix = "[new]", tempretry = 0) :
        if dirfd == None :
            dirfd = AT_FDCWD
        #end if
        self.dirfd = dirfd
        self.pathname = _get_path(pathname, "pathname")
        self.fd = open_at \
          (
            dirfd = dirfd,
            pathname = os.path.split(os.path.abspath(pathname))[0],
              # ensure it’s on same filesystem as file it’ll be replacing
            flags = (os.O_WRONLY, os.O_RDWR)[readable] | os.O_TMPFILE,
            mode = mode
          )
        self.tempsuffix = _get_path(tempsuffix, "temp suffix")
        if not isinstance(tempretry, int) or tempretry < 0 :
            raise ValueError("tempretry must be non-negative integer")
        #end if
        self.tempretry = tempretry
    #end __init__

    def open(self, mode) :
        "opens a new file object for writing new file contents."
        return \
            os.fdopen(os.dup(self.fd), mode, closefd = True)
    #end open

    def save(self) :
        "makes the new file visible in the filesystem, replacing any existing" \
        " file with the same name."
        assert self.fd != None, "file already closed"
        retry = self.tempretry
        retry_index = 0
        tempsuffix = self.tempsuffix
        while True :
            temppathname = self.pathname + tempsuffix
            try :
                save_tmpfile(self.fd, temppathname)
            except FileExistsError :
                if retry_index == retry :
                    raise
                #end if
            else :
                break
            #end try
            retry_index += 1
            tempsuffix = self.tempsuffix + b"[%d]" % retry_index
        #end while
        try :
            rename_at(self.dirfd, temppathname, self.dirfd, self.pathname, RENAME_EXCHANGE)
        except FileNotFoundError :
            # no existing file with that name
            rename_at(self.dirfd, temppathname, self.dirfd, self.pathname, RENAME_NOREPLACE)
        else :
            os.unlink(temppathname) # this is now the old file
        #end try
        os.close(self.fd)
        self.fd = None
    #end save

    def unsave(self) :
        "explicitly abandons the saving of the output file."
        if self.fd != None :
            os.close(self.fd)
            self.fd = None
        #end if
    #end unsave

    __del__ = unsave

#end TentativeFile

#+
# Overall
#-

def _atexit() :
    # disable all __del__ methods at process termination to avoid segfaults
    for cls in TentativeFile, :
        delattr(cls, "__del__")
    #end for
#end _atexit
atexit.register(_atexit)
del _atexit
