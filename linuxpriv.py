"""This module implements a pure-Python wrapper around various
Linux-specific privilege-control functions."""
#+
# Copyright 2024 Lawrence D'Oliveiro <ldo@geek-central.gen.nz>.
# Licensed under the GNU Lesser General Public License v2.1 or later.
#-

import os
import ctypes as ct
from weakref import \
    ref as weak_ref, \
    WeakValueDictionary
import atexit

from linuxfs import \
    def_syscall, \
    _BIT_FLAG, \
    _get_fileno, \
    _check_sts, \
    libc
from linuxproc import \
    set_no_new_privs

#+
# Low-level definitions
#-

class SYS :
    "syscall codes."
    # actual numeric syscall codes (__NR_xxx) can be found in /usr/include/asm-generic/unistd.h.
    # /usr/include/bits/syscall.h just defines SYS_xxx synonyms for these.
    landlock_create_ruleset = 444
    landlock_add_rule = 445
    landlock_restrict_self = 446
#end SYS

class LANDLOCK :
    # from /usr/include/linux/landlock.h:

    CREATE_RULESET_VERSION = 1
        # for trick call to landlock_create_ruleset to
        # return ABI version instead of creating a ruleset

    class ruleset_attr(ct.Structure) :
        _fields_ = \
            [
                ("handled_access_fs", ct.c_uint64),
                # following since Landlock version 4:
                ("handled_access_net", ct.c_uint64),
                # following since Landlock version 6:
                ("scoped", ct.c_uint64),
            ]
    #end ruleset_attr

    # types of rule:
    RULE_PATH_BENEATH = 1 # filesystem access rule
    RULE_NET_PORT = 2 # network port access rule

    class path_beneath_attr(ct.Structure) :
        _fields_ = \
            [
                ("allowed_access", ct.c_uint64),
                ("parent_fd", ct.c_int32),
            ]
    #end path_beneath_attr

    class net_port_attr(ct.Structure) :
        _fields_ = \
            [
                ("allowed_access", ct.c_uint64),
                ("port", ct.c_uint64),
            ]
    #end net_port_attr

    # filesystem flags
    # since Landlock version 1:
    ACCESS_FS_EXECUTE = 1
    ACCESS_FS_WRITE_FILE = 2
    ACCESS_FS_READ_FILE = 4
    ACCESS_FS_READ_DIR = 8
    ACCESS_FS_REMOVE_DIR = 16
    ACCESS_FS_REMOVE_FILE = 32
    ACCESS_FS_MAKE_CHAR = 64
    ACCESS_FS_MAKE_DIR = 128
    ACCESS_FS_MAKE_REG = 256
    ACCESS_FS_MAKE_SOCK = 512
    ACCESS_FS_MAKE_FIFO = 1024
    ACCESS_FS_MAKE_BLOCK = 2048
    ACCESS_FS_MAKE_SYM = 4096
    # since Landlock version 2:
    ACCESS_FS_REFER = 8192
    # since Landlock version 3:
    ACCESS_FS_TRUNCATE = 16384
    # since Landlock version 5:
    ACCESS_FS_IOCTL_DEV = 32768

    # network flags (since Landlock version 4)
    ACCESS_NET_BIND_TCP = 1
    ACCESS_NET_CONNECT_TCP = 2

    # scope flags (since Landlock version 6)
    SCOPE_ABSTRACT_UNIX_SOCKET = 1
      # no access to abstract Unix sockets created outside Landlock domain
    SCOPE_SIGNAL = 2
      # no sending signals to processes outside Landlock domain

#end LANDLOCK

class _LL_ACCESS(_BIT_FLAG) :
    # subclasses have a version attribute indicating their applicability
    # under various versions of Landlock.

    @property
    def value(self) :
        # prevent superclasses seeing anything other than bit value
        return \
            self._value_[0]
    #end value

    @property
    def min_version(self) :
        "minimum Landlock version to which this flag is applicable."
        return \
            self._value_[1]
    #end min_version

    def __str__(self) :
        return \
            "<%s.%s: %d>" % (type(self).__name__, self.name, self.value)
    #end __str__
    __repr__ = __str__ # set.__str__ seems to use __repr__ instead of __str__

#end _LL_ACCESS

landlock_create_ruleset = def_syscall \
  (
    "landlock_create_ruleset",
    SYS.landlock_create_ruleset,
    (ct.c_void_p, ct.c_size_t, ct.c_uint32),
    ct.c_int
  )
landlock_add_rule = def_syscall \
  (
    "landlock_add_rule",
    SYS.landlock_add_rule,
    (ct.c_int, ct.c_uint, ct.c_void_p, ct.c_uint32),
    ct.c_int
  )
landlock_restrict_self = def_syscall \
  (
    "landlock_restrict_self",
    SYS.landlock_restrict_self,
    (ct.c_int, ct.c_uint32),
    ct.c_int
  )

#+
# Higher-level stuff
#-

def _check_errres(res) :
    if res < 0 :
        errn =  - res
        raise OSError(errn, os.strerror(errn))
    #end if
    return \
        res
#end _check_errres

def get_landlock_version() :
    return \
        _check_errres(landlock_create_ruleset(None, 0, LANDLOCK.CREATE_RULESET_VERSION))
#end get_landlock_version

class Landlock :
    "high-level wrapper around the landlock(7) functions. Do not instantiate" \
    " directly; use the create() function to create a class instance that" \
    " wraps a new ruleset. Call the add_rule_fs() and add_rule_net() methods" \
    " to add filesystem or network rules respectively to the ruleset, then" \
    " the restrict_self() method to apply the ruleset to the current thread." \
    "\n" \
    "The add_rule_xx methods return self, to allow convenient method chaining;" \
    " as another convenience, the rule args may be specified directly to the" \
    " create call."

    __slots__ = ("fd", "__weakref__") # to forestall typos

    _instances = WeakValueDictionary()

    class ACCESS_FS(_LL_ACCESS) :
        "filesystem access attributes. There are additional convenience" \
        " attribute flags indicating file ops, directory ops, ops which" \
        " change things and ops on special files."

        # (bitnr, min_version, dir_op, write_op, special_file_op)
        EXECUTE = (0, 1, False, False, False)
        WRITE_FILE = (1, 1, False, True, False)
        READ_FILE = (2, 1, False, False, False)
        READ_DIR = (3, 1, True, False, False)
        REMOVE_DIR = (4, 1, True, True, False)
        REMOVE_FILE = (5, 1, True, True, False)
        MAKE_CHAR = (6, 1, True, True, True)
        MAKE_DIR = (7, 1, True, True, False)
        MAKE_REG = (8, 1, True, True, False)
        MAKE_SOCK = (9, 1, True, True, True)
        MAKE_FIFO = (10, 1, True, True, True)
        MAKE_BLOCK = (11, 1, True, True, True)
        MAKE_SYM = (12, 1, True, True, False)
        REFER = (13, 2, True, True, False)
        TRUNCATE = (14, 3, False, True, False)
        IOCTL_DEV = (15, 5, False, True, True)

        @property
        def file_op(self) :
            "does this affect operations on the contents of files."
            return \
                not self._value_[2]
        #end file_op

        @property
        def dir_op(self) :
            "does this affect operations on the contents of directories."
            return \
                self._value_[2]
        #end dir_op

        @property
        def write_op(self) :
            "does this affect operations that make changes to the contents of files/directories."
            return \
                self._value_[3]
        #end write_op

        @property
        def special_file_op(self) :
            "does this affect operations on special files."
            return \
                self._value_[4]
        #end special_file_op

    #end ACCESS_FS

    class ACCESS_NET(_LL_ACCESS) :
        "network port access attributes."

        # (bitnr, min_version)
        BIND_TCP = (0, 4)
        CONNECT_TCP = (1, 4)

    #end ACCESS_NET

    class SCOPE(_LL_ACCESS) :
        "IPC scope attributes."

        # (bitnr, min_version)
        ABSTRACT_UNIX_SOCKET = (0, 6)
        SIGNAL = (1, 6)

    #end SCOPE

    def __new__(celf, _fd) :
        self = celf._instances.get(_fd)
        if self == None :
            self = super().__new__(celf)
            self.fd = _fd
            celf._instances[_fd] = self
        #end if
        return \
            self
    #end __new__

    def close(self) :
        if self.fd != None :
            os.close(self.fd)
            self.fd = None
        #end if
    #end close

    __del__ = close

    @classmethod
    def create(celf, *, fs_handled = None, net_handled = None, scoped = None, fs_rules = None, net_rules = None) :
        if (
                    fs_rules != None
                and
                    not
                        (
                            isinstance(fs_rules, (list, tuple))
                        and
                            all(isinstance(i, (list, tuple)) and len(i) == 2 for i in fs_rules)
                        )
            or
                    net_rules != None
                and
                    not
                        (
                            isinstance(net_rules, (list, tuple))
                        and
                            all(isinstance(i, (list, tuple)) and len(i) == 2 for i in net_rules)
                        )
        ) :
            raise TypeError \
              (
                "fs_rules and net_rules args, if specified, must be"
                " tuples of («allowed_access», «target») pairs"
              )
        #end if
        attr = LANDLOCK.ruleset_attr()
        if fs_handled != None :
            attr.handled_access_fs = celf.ACCESS_FS.encode_mask(fs_handled)
        #end if
        if net_handled != None :
            attr.handled_access_net = celf.ACCESS_NET.encode_mask(net_handled)
        #end if
        if scoped != None :
            attr.scoped = celf.SCOPE.encode_mask(scoped)
        #end if
        res = _check_errres(landlock_create_ruleset(ct.byref(attr), ct.sizeof(attr), 0))
        self = celf(res)
        if fs_rules != None :
            for item in fs_rules :
                self.add_rule_fs(*item)
            #end for
        #end if
        if net_rules != None :
            for item in net_rules :
                self.add_rule_net(*item)
            #end for
        #end if
        return \
            self
    #end create

    def add_rule_fs(self, allowed_access, parent) :
        "adds a filesystem rule to the ruleset. allowed_access is" \
        " either a bitmask or a set of Landlock.ACCESS_FS items," \
        " while parent is either a file object, an fd or a path name."
        opened_fd = None
        try :
            if isinstance(parent, (bytes, str)) :
                opened_fd = os.open(parent, os.O_PATH | os.O_CLOEXEC)
                parent_fd = opened_fd
            else :
                parent_fd = _get_fileno(parent)
            #end if
            attr = LANDLOCK.path_beneath_attr \
              (
                allowed_access = type(self).ACCESS_FS.encode_mask(allowed_access),
                parent_fd = parent_fd
              )
            _check_errres(landlock_add_rule(self.fd, LANDLOCK.RULE_PATH_BENEATH, ct.byref(attr), 0))
        finally :
            if opened_fd != None :
                os.close(opened_fd)
            #end if
        #end try
        return \
            self
    #end add_rule_fs

    def add_rule_net(self, allowed_access, port) :
        "adds a network rule to the ruleset. allowed_access is" \
        " either a bitmask or a set of Landlock.ACCESS_NET items," \
        " while port is an integer port number."
        attr = LANDLOCK.net_port_attr \
          (
            allowed_access = type(self).ACCESS_NET.encode_mask(allowed_access),
            port = port
          )
        _check_errres(landlock_add_rule(self.fd, LANDLOCK.RULE_NET_PORT, ct.byref(attr), 0))
        return \
            self
    #end add_rule_net

    def restrict_self(self) :
        "applies the ruleset to the current thread."
        _check_errres(landlock_restrict_self(self.fd, 0))
        # don’t bother returning self because this will typically be the end
        # of use of the object
    #end restrict_self

#end Landlock

#+
# Overall
#-

def _atexit() :
    # disable all __del__ methods at process termination to avoid segfaults
    for cls in Landlock, :
        delattr(cls, "__del__")
    #end for
#end _atexit
atexit.register(_atexit)
del _atexit
