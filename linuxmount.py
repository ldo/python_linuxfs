"""This module implements a pure-Python wrapper around various
Linux-specific filesystem-mount functions."""
#+
# Copyright 2024 Lawrence D'Oliveiro <ldo@geek-central.gen.nz>.
# Licensed under the GNU Lesser General Public License v2.1 or later.
#-

import os
from weakref import \
    WeakValueDictionary
import ctypes as ct
import atexit
from linuxfs import \
    libc, \
    _BIT_FLAG, \
    _get_fileno, \
    _get_path, \
    _check_sts

class MOUNT :

    # from /usr/include/linux/mount.h:

    MS_RDONLY = 1
    MS_NOSUID = 2
    MS_NODEV  = 4
    MS_NOEXEC = 8
    MS_SYNCHRONOUS = 16
    MS_REMOUNT = 32
    MS_MANDLOCK = 64
    MS_DIRSYNC = 128
    MS_NOSYMFOLLOW = 256
    MS_NOATIME = 1024
    MS_NODIRATIME = 2048
    MS_BIND = 4096
    MS_MOVE = 8192
    MS_REC = 16384
    MS_VERBOSE = 32768
    MS_SILENT = 32768
    MS_POSIXACL = 1 << 16
    MS_UNBINDABLE = 1 << 17
    MS_PRIVATE = 1 << 18
    MS_SLAVE = 1 << 19
    MS_SHARED = 1 << 20
    MS_RELATIME = 1 << 21
    MS_KERNMOUNT = 1 << 22
    MS_I_VERSION =  1 << 23
    MS_STRICTATIME = 1 << 24
    MS_LAZYTIME = 1 << 25
    MS_SUBMOUNT = 1 << 26
    MS_NOREMOTELOCK = 1 << 27
    MS_NOSEC = 1 << 28
    MS_BORN = 1 << 29
    MS_ACTIVE = 1 << 30
    MS_NOUSER = 1 << 31

    MS_RMT_MASK = MS_RDONLY | MS_SYNCHRONOUS | MS_MANDLOCK | MS_I_VERSION | MS_LAZYTIME

    MS_MGC_VAL = 0xC0ED0000
    MS_MGC_MSK = 0xffff0000

    OPEN_TREE_CLONE = 1
    OPEN_TREE_CLOEXEC = os.O_CLOEXEC

    MOVE_MOUNT_F_SYMLINKS = 0x00000001
    MOVE_MOUNT_F_AUTOMOUNTS = 0x00000002
    MOVE_MOUNT_F_EMPTY_PATH = 0x00000004
    MOVE_MOUNT_T_SYMLINKS = 0x00000010
    MOVE_MOUNT_T_AUTOMOUNTS = 0x00000020
    MOVE_MOUNT_T_EMPTY_PATH = 0x00000040
    MOVE_MOUNT_SET_GROUP = 0x00000100
    MOVE_MOUNT_BENEATH = 0x00000200
    MOVE_MOUNT__MASK = 0x00000377

    FSOPEN_CLOEXEC = 0x00000001

    FSPICK_CLOEXEC = 0x00000001
    FSPICK_SYMLINK_NOFOLLOW = 0x00000002
    FSPICK_NO_AUTOMOUNT = 0x00000004
    FSPICK_EMPTY_PATH = 0x00000008

    fsconfig_command = ct.c_uint
    # values for fsconfig_command:
    FSCONFIG_SET_FLAG = 0
    FSCONFIG_SET_STRING = 1
    FSCONFIG_SET_BINARY = 2
    FSCONFIG_SET_PATH = 3
    FSCONFIG_SET_PATH_EMPTY = 4
    FSCONFIG_SET_FD = 5
    FSCONFIG_CMD_CREATE = 6
    FSCONFIG_CMD_RECONFIGURE = 7
    FSCONFIG_CMD_CREATE_EXCL = 8 # kernel 6.6 or later

    FSMOUNT_CLOEXEC = 0x00000001

    MOUNT_ATTR_RDONLY = 0x00000001
    MOUNT_ATTR_NOSUID = 0x00000002
    MOUNT_ATTR_NODEV = 0x00000004
    MOUNT_ATTR_NOEXEC = 0x00000008
    MOUNT_ATTR__ATIME = 0x00000070
    MOUNT_ATTR_RELATIME = 0x00000000
    MOUNT_ATTR_NOATIME = 0x00000010
    MOUNT_ATTR_STRICTATIME = 0x00000020
    MOUNT_ATTR_NODIRATIME = 0x00000080
    MOUNT_ATTR_IDMAP = 0x00100000
    MOUNT_ATTR_NOSYMFOLLOW = 0x00200000

    class mount_attr(ct.Structure) :
        _fields_ = \
            [
                ("attr_set", ct.c_uint64),
                ("attr_clr", ct.c_uint64),
                ("propagation", ct.c_uint64),
                ("userns_fd", ct.c_uint64),
            ]
    #end mount_attr

    MOUNT_ATTR_SIZE_VER0 = 32

    class statmount(ct.Structure) :
        _fields_ = \
            [
                ("size", ct.c_uint32),
                ("mnt_opts", ct.c_uint32),
                ("mask", ct.c_uint64),
                ("sb_dev_major", ct.c_uint32),
                ("sb_dev_minor", ct.c_uint32),
                ("sb_magic", ct.c_uint64),
                ("sb_flags", ct.c_uint32),
                ("fs_type", ct.c_uint32),
                ("mnt_id", ct.c_uint64),
                ("mnt_parent_id", ct.c_uint64),
                ("mnt_id_old", ct.c_uint32),
                ("mnt_parent_id_old", ct.c_uint32),
                ("mnt_attr", ct.c_uint64),
                ("mnt_propagation", ct.c_uint64),
                ("mnt_peer_group", ct.c_uint64),
                ("mnt_master", ct.c_uint64),
                ("propagate_from", ct.c_uint64),
                ("mnt_root", ct.c_uint32),
                ("mnt_point", ct.c_uint32),
                ("mnt_ns_id", ct.c_uint64),
                ("__spare2", ct.c_uint64 * 49),
                ("str", 0 * ct.c_char),
            ]
    #end statmount

    class mnt_id_req(ct.Structure) :
        _fields_ = \
            [
                ("size", ct.c_uint32),
                ("spare", ct.c_uint32),
                ("mnt_id", ct.c_uint64),
                ("param", ct.c_uint64),
                ("mnt_ns_id", ct.c_uint64),
            ]
    #end mnt_id_req

    MNT_ID_REQ_SIZE_VER0 = 24
    MNT_ID_REQ_SIZE_VER1 = 32

    STATMOUNT_SB_BASIC = 0x00000001
    STATMOUNT_MNT_BASIC = 0x00000002
    STATMOUNT_PROPAGATE_FROM = 0x00000004
    STATMOUNT_MNT_ROOT = 0x00000008
    STATMOUNT_MNT_POINT = 0x00000010
    STATMOUNT_FS_TYPE = 0x00000020
    STATMOUNT_MNT_NS_ID = 0x00000040
    STATMOUNT_MNT_OPTS = 0x00000080

    LSMT_ROOT = 0xffffffffffffffff
    LISTMOUNT_REVERSE = 1 << 0

    # from /usr/include/x86_64-linux-gnu/sys/mount.h:

    MNT_FORCE = 1
    MNT_DETACH = 2
    MNT_EXPIRE = 4
    UMOUNT_NOFOLLOW = 8

#end MOUNT

# following calls are available in Debian 11 and later glibc, so I
# don’t bother defining my own syscalls
libc.open_tree.restype = ct.c_int
libc.open_tree.argtypes = (ct.c_int, ct.c_char_p, ct.c_uint)
libc.move_mount.restype = ct.c_int
libc.move_mount.argtypes = (ct.c_int, ct.c_char_p, ct.c_int, ct.c_char_p, ct.c_uint)
libc.fsopen.restype = ct.c_int
libc.fsopen.argtypes = (ct.c_char_p, ct.c_uint)
libc.fsconfig.restype = ct.c_int
libc.fsconfig.argtypes = (ct.c_int, ct.c_uint, ct.c_char_p, ct.c_void_p, ct.c_int)
libc.fsmount.restype = ct.c_int
libc.fsmount.argtypes = (ct.c_int, ct.c_uint, ct.c_uint)
libc.fspick.restype = ct.c_int
libc.fspick.argtypes = (ct.c_int, ct.c_char_p, ct.c_uint)
libc.pivot_root.argtypes = (ct.c_char_p, ct.c_char_p)
libc.pivot_root.restype = ct.c_int
libc.umount2.argtypes = (ct.c_char_p, ct.c_int) # superset of libc.umount
libc.umount2.restype = ct.c_int
libc.mount_setattr.restype = ct.c_int
libc.mount_setattr.argtypes = (ct.c_int, ct.c_char_p, ct.c_uint, ct.c_void_p, ct.c_size_t)

class FSPICK(_BIT_FLAG) :

    CLOEXEC = 0
    SYMLINK_NOFOLLOW = 1
    NO_AUTOMOUNT = 2
    EMPTY_PATH = 3

#end FSPICK

class MOUNT_ATTR(_BIT_FLAG) :

    RDONLY = 0
    NOSUID = 1
    NODEV = 2
    NOEXEC = 3
    A__ATIME = (6, 5, 4)
    RELATIME = ()
    NOATIME = 4
    STRICTATIME = 5
    NODIRATIME = 7
    IDMAP = 20
    NOSYMFOLLOW = 21

#end MOUNT_ATTR

class OPEN_TREE(_BIT_FLAG) :

    CLONE = 0
    CLOEXEC = 19
    NO_AUTOMOUNT = 11
    EMPTY_PATH = 12
    RECURSIVE = 15
    SYMLINK_FOLLOW = 10

#end OPEN_TREE

class MOVE_MOUNT(_BIT_FLAG) :

    F_SYMLINKS = 0
    F_AUTOMOUNTS = 1
    F_EMPTY_PATH = 2
    T_SYMLINKS = 4
    T_AUTOMOUNTS = 5
    T_EMPTY_PATH = 6
    SET_GROUP = 8
    BENEATH = 9

#end MOVE_MOUNT

class SETATTR(_BIT_FLAG) :

    NO_AUTOMOUNT = 11
    EMPTY_PATH = 12
    RECURSIVE = 15
    SYMLINK_FOLLOW = 10

#end SETATTR

class PROPAGATE(_BIT_FLAG) :

    UNBINDABLE = 17
    PRIVATE = 18
    SLAVE = 19
    SHARED = 20

#end PROPAGATE

class UMOUNT(_BIT_FLAG) :

    FORCE = 0
    DETACH = 1
    EXPIRE = 2
    NOFOLLOW = 3

#end UMOUNT

class FSConfig :
    "a wrapper around a file descriptor created by fsopen(2) or fspick(2)." \
    " You can instantiate this yourself around a suitable fd, or you can call" \
    " the new() or pick() methods to make the requisite syscalls and wrap the" \
    " result for you."

    __slots__ = ("fd", "_ownfd", "__weakref__") # to forestall typos
    _instances = WeakValueDictionary()

    def __new__(celf, fd, ownfd : bool = True) :
        fd = _get_fileno(fd)
        self = celf._instances.get(fd)
        if self == None :
            self = super().__new__(celf)
            self.fd = fd
            self._ownfd = ownfd
            celf._instances[fd] = self
        else :
            assert self._ownfd == ownfd, "inconsistent setting of ownfd flag"
        #end if
        return \
            self
    #end __new__

    def fileno(self) :
        return \
            self.fd
    #end fileno

    @classmethod
    def open(celf, fsname, *, cloexec : bool) :
        c_flags = (0, MOUNT.FSOPEN_CLOEXEC)[cloexec]
        res = libc.fsopen(_get_path(fsname, "fsname"), c_flags)
        _check_sts(res)
        return \
            celf(res)
    #end open

    @classmethod
    def pick(celf, dirfd, pathname, flags) :
        c_flags = FSPICK.encode_mask(flags)
        res = libc.fspick(_get_fileno(dirfd, "dirfd"), _get_path(pathname, "pathname"), c_flags)
        _check_sts(res)
        return \
            celf(res)
    #end pick

    def close(self) :
        if self.fd != None and self._ownfd :
            os.close(self.fd)
        #end if
        self.fd = None
    #end close

    __del__ = close

    def set_flag(self, key) :
        c_key = _get_path(key, "key")
        _check_sts(libc.fsconfig(self.fd, MOUNT.FSCONFIG_SET_FLAG, c_key, None, 0))
        return \
            self
    #end set_flag

    def set_string(self, key, value) :
        c_key = _get_path(key, "key")
        c_value = _get_path(value, "value")
        _check_sts(libc.fsconfig(self.fd, MOUNT.FSCONFIG_SET_STRING, c_key, c_value, 0))
        return \
            self
    #end set_string

    def set_binary(self, key, value) :
        c_key = _get_path(key, "key")
        if not isinstance(value, (bytes, bytearray)) :
            raise TypeError("value must be bytes or bytearray, not %s" % type(value).__name__)
        #end if
        _check_sts(libc.fsconfig(self.fd, MOUNT.FSCONFIG_SET_BINARY, c_key, value, 0))
        return \
            self
    #end set_binary

    def set_path(self, key, value, dirfd) :
        c_key = _get_path(key, "key")
        c_value = _get_path(value, "value")
        if dirfd != None :
            c_fd = _get_fileno(dirfd, "dirfd")
            cmd = MOUNT.FSCONFIG_SET_PATH
        else :
            c_fd = 0
            cmd = MOUNT.FSCONFIG_SET_PATH_EMPTY
        #end if
        _check_sts(libc.fsconfig(self.fd, cmd, c_key, c_value, c_fd))
        return \
            self
    #end set_path

    def set_fd(self, key, value) :
        c_key = _get_path(key, "key")
        c_fd = _get_fileno(value, "value")
        _check_sts(libc.fsconfig(self.fd, MOUNT.FSCONFIG_SET_FD, c_key, None, c_fd))
        return \
            self
    #end set_fd

    def createfs(self, *, excl : bool) :
        cmd = (MOUNT.FSCONFIG_CMD_CREATE, MOUNT.FSCONFIG_CMD_CREATE_EXCL)[excl]
        _check_sts(libc.fsconfig(self.fd, cmd, None, None, 0))
        return \
            self
    #end createfs

    def reconfigure(self) :
        _check_sts(libc.fsconfig(self.fd, MOUNT.FSCONFIG_CMD_RECONFIGURE, None, None, 0))
        return \
            self
    #end reconfigure

    def mount(self, *, mount_attrs, cloexec : bool) :
        c_flags = (0, MOUNT.FSMOUNT_CLOEXEC)[cloexec]
        c_mount_attrs = MOUNT_ATTR.encode_mask(mount_attrs)
        res = libc.fsmount(self.fd, c_flags, c_mount_attrs)
        _check_sts(res)
        return \
            FSMount(res)
    #end mount

#end FSConfig

class FSMount :
    "a wrapper around a file descriptor created by open_tree(2) or fsmount(2)." \
    " You can instantiate this yourself around a suitable fd, or you can call" \
    " the open_tree() method or FSConfig.mount() to make the requisite syscalls" \
    " and wrap the result for you."

    __slots__ = ("fd", "_ownfd", "__weakref__") # to forestall typos
    _instances = WeakValueDictionary()

    def __new__(celf, fd, ownfd : bool = True) :
        fd = _get_fileno(fd)
        self = celf._instances.get(fd)
        if self == None :
            self = super().__new__(celf)
            self.fd = fd
            self._ownfd = ownfd
            celf._instances[fd] = self
        else :
            assert self._ownfd == ownfd, "inconsistent setting of ownfd flag"
        #end if
        return \
            self
    #end __new__

    def fileno(self) :
        return \
            self.fd
    #end fileno

    @classmethod
    def open_tree(celf, dirfd, pathname, flags) :
        c_flags = OPEN_TREE.encode_mask(flags)
        res = libc.open_tree(_get_fileno(dirfd, "dirfd"), _get_path(pathname, "pathname"), c_flags)
        _check_sts(res)
        return \
            celf(res)
    #end open_tree

    def close(self) :
        if self.fd != None and self._ownfd :
            os.close(self.fd)
        #end if
        self.fd = None
    #end close

    __del__ = close

    def setattr(self, flags, *, attr_set = None, attr_clr = None, propagation = None, userns_fd = None) :
        mount_setattr \
          (
            self, b"", flags,
            attr_set = attr_set,
            attr_clr = attr_clr,
            propagation = propagation,
            userns_fd = userns_fd
          )
    #end setattr

    def move_to(self, to_dirfd, to_pathname, flags) :
        "moves this mount to the specified destination. Note the MOVE_MOUNT_F_xxx" \
        " flags are disallowed, since they are automatically specified as appropriate."
        c_flags = MOVE_MOUNT.encode_mask(flags)
        disallowed = \
            (
                c_flags
            &
                MOVE_MOUNT.encode_mask
                  (
                    {MOVE_MOUNT.F_EMPTY_PATH, MOVE_MOUNT.F_AUTOMOUNTS, MOVE_MOUNT.F_SYMLINKS}
                  )
            )
        if disallowed != 0 :
            raise ValueError("flags not allowed: %s" % MOVE_MOUNT.make_set(disallowed))
        #end if
        c_flags |= MOVE_MOUNT.encode_mask({MOVE_MOUNT.F_EMPTY_PATH, MOVE_MOUNT.F_AUTOMOUNTS})
        move_mount(self, b"", to_dirfd, to_pathname, c_flags)
        return \
            self
    #end move_to

#end FSMount

def move_mount(from_dirfd, from_pathname, to_dirfd, to_pathname, flags) :
    c_flags = MOVE_MOUNT.encode_mask(flags)
    _check_sts \
      (
        libc.move_mount
          (
            _get_fileno(from_dirfd, "from_dirfd"), _get_path(from_pathname, "from_pathname"),
            _get_fileno(to_dirfd, "to_dirfd"), _get_path(to_pathname, "to_pathname"),
            c_flags
          )
      )
#end move_mount

def pivot_root(new_root, old_root) :
    c_new_root = _get_path(new_root)
    c_old_root = _get_path(old_root)
    _check_sts(libc.pivot_root(c_new_root, c_old_root))
#end pivot_root

def umount(target, flags) :
    c_target = _get_path(target)
    c_flags = UMOUNT.encode_mask(flags)
    _check_sts(libc.umount2(c_target, c_flags))
#end umount

def mount_setattr(dirfd, pathname, flags, *, attr_set = None, attr_clr = None, propagation = None, userns_fd = None) :
    c_flags = MOUNT_ATTR.encode_mask(flags)
    c_attr = MOUNT.mount_attr()
    if attr_set != None :
        c_attr.attr_set = SETATTR.encode_mask(attr_set)
    #end if
    if attr_clr != None :
        c_attr.attr_clr = SETATTR.encode_mask(attr_clr)
    #end if
    if propagation != None :
        c_attr.propagation = PROPAGATE.encode_mask(propagation)
    #end if
    if userns_fd != None :
        c_attr.userns_fd = _get_fileno(userns_fd, "userns_fd")
    #end if
    _check_sts \
      (
        libc.mount_setattr
          (
            _get_fileno(dirfd, "dirfd"), get_path(pathname),
            c_flags, ct.byref(c_attr), ct.sizeof(c_attr)
          )
      )
#end mount_setattr

#+
# Overall
#-

def _atexit() :
    # disable all __del__ methods at process termination to avoid segfaults
    for cls in FSConfig, FSMount :
        delattr(cls, "__del__")
    #end for
#end _atexit
atexit.register(_atexit)
del _atexit
