This repo provides pure-Python wrappers around some Linux-specific
system calls that have not (yet) found their way into the standard
Python library. Or they might be in the library, but my bindings
are more convenient in some way.

The wrappers are split into four modules:

  * `linuxfs` -- file/directory functions and common utilities used by
    other modules
  * `linuxmount` -- enhanced Linux mount API
  * `linuxpriv` -- privilege control, i.e. the Linux landlock API
  * `linuxproc` -- process control: `prctl` (selected), `pidfd`,
    `signalfd` and signal mask sets, namespaces

Examples of how to use these modules can be found in my
`python_linuxfs_examples` repo, available on
[GitLab](https://gitlab.com/ldo/python_linuxfs_examples) and
[BitBucket](https://bitbucket.org/ldo17/python_linuxfs_examples).

Lawrence D'Oliveiro <ldo@geek-central.gen.nz>
2025 January 31
