"""This module implements a pure-Python wrapper around various
Linux-specific process-control functions."""
#+
# Copyright 2024-2025 Lawrence D'Oliveiro <ldo@geek-central.gen.nz>.
# Licensed under the GNU Lesser General Public License v2.1 or later.
#-

import os
from weakref import \
    WeakValueDictionary
import enum
import ctypes as ct
import asyncio
import atexit

from linuxfs import \
    _IOC, \
    _BIT_FLAG, \
    _get_fileno, \
    _check_sts, \
    libc

if not hasattr(ct, "c_pid_t") :
    ct.c_pid_t = ct.c_int # correct for x86, at least
#end if

class PRCTL :
    "only the prctl(2) codes I need."
    # from </usr/include/linux/prctl.h> :
    SET_PDEATHSIG = 1
    SET_NO_NEW_PRIVS = 38
#end PRCTL

PIDFS_IOCTL_MAGIC = 0xFF
class PIDFD :
    "definitions from /usr/include/linux/pidfd.h and /usr/include/x86_64-linux-gnu/sys/pidfd.h"

    NONBLOCK = os.O_NONBLOCK
    THREAD = os.O_EXCL

    _IO = lambda nr : _IOC.IO(PIDFS_IOCTL_MAGIC, nr)
    GET_CGROUP_NAMESPACE = _IO(1)
    GET_IPC_NAMESPACE = _IO(2)
    GET_MNT_NAMESPACE = _IO(3)
    GET_NET_NAMESPACE = _IO(4)
    GET_PID_NAMESPACE = _IO(5)
    GET_PID_FOR_CHILDREN_NAMESPACE = _IO(6)
    GET_TIME_NAMESPACE = _IO(7)
    GET_TIME_FOR_CHILDREN_NAMESPACE = _IO(8)
    GET_USER_NAMESPACE = _IO(9)
    GET_UTS_NAMESPACE = _IO(10)
    del _IO

#end PIDFD

libc.prctl.argtypes = (ct.c_int, ct.c_ulong, ct.c_ulong, ct.c_ulong, ct.c_ulong)
libc.prctl.restype = ct.c_int
# Python 3.12 and later provide pidfd_open, setns and unshare
# in standard os module, but flags must be specified as bitmasks
libc.pidfd_open.argtypes = (ct.c_pid_t, ct.c_uint)
libc.pidfd_open.restype = ct.c_int
libc.pidfd_getfd.argtypes = (ct.c_int, ct.c_int, ct.c_uint)
libc.pidfd_getfd.restype = ct.c_int
libc.pidfd_send_signal.argtypes = (ct.c_int, ct.c_int, ct.c_void_p, ct.c_uint)
libc.pidfd_send_signal.restype = ct.c_int
libc.setns.argtypes = (ct.c_int, ct.c_int)
libc.setns.restype = ct.c_int
libc.unshare.argtypes = (ct.c_int,)
libc.unshare.restype = ct.c_int

libc.signalfd.argtypes = (ct.c_int, ct.c_void_p, ct.c_int)
libc.signalfd.restype = ct.c_int
libc.sigemptyset.argtypes = (ct.c_void_p,)
libc.sigemptyset.restype = ct.c_int
libc.sigfillset.argtypes = (ct.c_void_p,)
libc.sigfillset.restype = ct.c_int
libc.sigaddset.argtypes = (ct.c_void_p, ct.c_int)
libc.sigaddset.restype = ct.c_int
libc.sigdelset.argtypes = (ct.c_void_p, ct.c_int)
libc.sigdelset.restype = ct.c_int
libc.sigismember.argtypes = (ct.c_void_p, ct.c_int)
libc.sigismember.restype = ct.c_int
libc.sigprocmask.argtypes = (ct.c_int, ct.c_void_p, ct.c_void_p)
libc.sigprocmask.restype = ct.c_int

def set_no_new_privs() :
    "irreversibly sets the NO_NEW_PRIVS flag on this process and any" \
    " subsequent children it might create."
    _check_sts(libc.prctl(PRCTL.SET_NO_NEW_PRIVS, 1, 0, 0, 0))
#end set_no_new_privs

def set_prdeath_sig(sig : int) :
    "sets the signal to be sent to this process on the death of its parent."
    _check_sts(libc.prctl(PRCTL.SET_PDEATHSIG, sig, 0, 0, 0))
#end set_prdeath_sig

class PidFile :
    "Wrapper around a file descriptor that references a process. You can" \
    " instantiate this yourself with an FD returned from pidfd_open(2), or" \
    " you can call PidFile.open() to create the FD for you from a given" \
    " PID or object (e.g. subprocess.Popen) with a “pid” attribute."

    __slots__ = ("fd", "_ownfd", "__weakref__") # to forestall typos
    _instances = WeakValueDictionary()

    def __new__(celf, fd, ownfd : bool = True) :
        fd = _get_fileno(fd)
        self = celf._instances.get(fd)
        if self == None :
            self = super().__new__(celf)
            self.fd = fd
            self._ownfd = ownfd
            celf._instances[fd] = self
        else :
            assert self._ownfd == ownfd, "inconsistent setting of ownfd flag"
        #end if
        return \
            self
    #end __new__

    def fileno(self) :
        assert self.fd != None
        return \
            self.fd
    #end fileno

    def close(self) :
        if self.fd != None :
            os.close(self.fd)
            self.fd = None
        #end if
    #end close

    __del__ = close

    @classmethod
    def open(celf, child, *, nonblock : bool = False) :
        "opens a PidFile referencing the process identified by child. This" \
        " can be an integer PID, or something like a subprocess.Popen instance" \
        " which has a pid attribute."
        if hasattr(child, "pid") :
            child_pid = child.pid
        elif isinstance(child, int) :
            child_pid = child
        else :
            raise TypeError("pass an integer PID or something with a pid attribute")
        #end if
        c_flags = (0, PIDFD.NONBLOCK)[nonblock]
        res = libc.pidfd_open(child_pid, c_flags)
        _check_sts(res)
        return \
            celf(res)
    #end open

    def getfd(self, targetfd : int) :
        "returns an FD referencing the same file description as the FD" \
        " targetfd within the target process."
        c_flags = 0 # reserved for future use
        res = libc.pidfd_getfd(self.fd, targetfd, c_flags)
        _check_sts(res)
        return \
            res
    #end getfd

    def send_signal(self, *, sig : int) :
        "note that the passing of a siginfo_t structure is not currently supported."
        c_flags = 0 # reserved for future use
        res = libc.pidfd_send_signal(self.fd, sig, None, c_flags)
        _check_sts(res)
    #end send_signal

#end PidFile

class CLONE(_BIT_FLAG) :
    "bits from /usr/include/linux/sched.h."
    # also found at (for x86-64) /usr/include/x86_64-linux-gnu/bits/sched.h
    NEWTIME = 7 # clashes with CSIGNAL mask
    VM = 8
    FS = 9
    FILES = 10
    SIGHAND = 11
    PIDFD = 12
    PTRACE = 13
    VFORK = 14
    PARENT = 15
    THREAD = 16
    NEWNS = 17
    SYSVSEM = 18
    SETTLS = 19
    PARENT_SETTID = 20
    CHILD_CLEARTID = 21
    DETACHED = 22
    UNTRACED = 23
    CHILD_SETTICD = 24
    NEWCGROUP = 25
    NEWUTS = 26
    NEWIPC = 27
    NEWUSER = 28
    NEWPID = 29
    NEWNET = 30
    IO = 31
    CLEAR_SIGHAND = 32
    INTO_CGROUP = 33
#end CLONE

def unshare(flags) :
    _check_sts(libc.unshare(CLONE.encode_mask(flags)))
#end unshare

def setns(fd, nstype) :
    _check_sts(libc.setns(_get_fileno(fd), CLONE.encode_mask(nstype)))
#end setns

class SIGNALFD :
    # definition of signalfd_siginfo, from either
    # /usr/include/linux/signalfd.h or /usr/include/«arch»/sys/signalfd.h

    class siginfo(ct.Structure) :
        _fields_ = \
            [
                ("ssi_signo", ct.c_uint32),
                ("ssi_errno", ct.c_int32),
                ("ssi_code", ct.c_int32),
                ("ssi_pid", ct.c_uint32),
                ("ssi_uid", ct.c_uint32),
                ("ssi_fd", ct.c_int32),
                ("ssi_tid", ct.c_uint32),
                ("ssi_band", ct.c_uint32),
                ("ssi_overrun", ct.c_uint32),
                ("ssi_trapno", ct.c_uint32),
                ("ssi_status", ct.c_int32),
                ("ssi_int", ct.c_int32),
                ("ssi_ptr", ct.c_uint64),
                ("ssi_utime", ct.c_uint64),
                ("ssi_stime", ct.c_uint64),
                ("ssi_addr", ct.c_uint64),
                ("ssi_addr_lsb", ct.c_uint16),
                ("__pad2", ct.c_uint16),
                ("ssi_syscall", ct.c_int32),
                ("ssi_call_addr", ct.c_uint64),
                ("ssi_arch", ct.c_uint32),
                ("__pad", ct.c_uint8 * 28),
            ]

        def __repr__(self) :
            celf = type(self)
            return \
                (
                    "%s(%s)"
                %
                    (
                        celf.__name__,
                        ", ".join
                          (
                                "%s=%s"
                            %
                                (f, repr(getattr(self, f)))
                            for f, ft in celf._fields_ if not f.startswith("_")
                          ),
                    )
                )
        #end __repr__

    #end siginfo

#end SIGNALFD

class SFD(_BIT_FLAG) :
    NONBLOCK = 11
    CLOEXEC = 19
#end SFD

class SIG(enum.IntEnum) :

    HUP = 1
    INT = 2
    QUIT = 3
    ILL = 4
    TRAP = 5
    ABRT = 6
    IOT = 6
    BUS = 7
    FPE = 8
    KILL = 9
    USR1 = 10
    SEGV = 11
    USR2 = 12
    PIPE = 13
    ALRM = 14
    TERM = 15
    STKFLT = 16
    CHLD = 17
    CONT = 18
    STOP = 19
    TSTP = 20
    TTIN = 21
    TTOU = 22
    URG = 23
    XCPU = 24
    XFSZ = 25
    VTALRM = 26
    PROF = 27
    WINCH = 28
    IO = 29

    # LOST = 29

    PWR = 30
    SYS = 31

    # RTMIN = 32

    @property
    def maskable(self) :
        "can this signal be caught, blocked or ignored."
        celf = type(self)
        return \
            self not in {celf.KILL, celf.STOP}
    #end maskable

#end SIG
SIG._NSIG = 64
SIG.POLL = SIG.IO
SIG.UNUSED = SIG.SYS

# op defs from either of </usr/include/asm-generic/signal-defs.h>
# or </usr/include/«arch»/bits/sigaction.h>
SIG_BLOCK = 0
SIG_UNBLOCK = 1
SIG_SETMASK = 2

class SigProcMask :
    "high-level wrapper around a sigset_t object. Note the" \
    " updater methods all end with “return self”, allowing" \
    " update calls to be chained."

    __slots__ = ("_mask",) # to forestall typos

    SIGSET_T_SIZE = 128 # I think this is correct across all architectures ...

    def __init__(self, initsigs = None) :
        self._mask = bytearray((0,) * self.SIGSET_T_SIZE)
        if initsigs != None :
            if (
                    not isinstance(initsigs, (set, frozenset))
                or
                    not all(isinstance(e, SIG) for e in initsigs)
            ) :
                raise TypeError("initsigs must be set of SIG")
            #end if
            for sig in initsigs :
                self.add(sig)
            #end for
        #end if
    #end __init__

    def __repr__(self) :
        return \
            (
                "%s({%s})"
            %
                (type(self).__name__, ", ".join("SIG.%s" % s.name for s in SIG if s in self))
            )
    #end __repr__

    @classmethod
    def get_cur(celf) :
        "returns a copy of the current signal mask in a new SigProcMask object."
        res = celf()
        _check_sts(libc.sigprocmask(SIG_SETMASK, None, res._mask_addr))
        return \
            res
    #end get_cur

    @property
    def _mask_addr(self) :
        # returns the memory address of the _mask object.
        return \
            ct.addressof((ct.c_ubyte * self.SIGSET_T_SIZE).from_buffer(self._mask))
    #end _mask_addr

    def empty(self) :
        "removes all signals from the mask."
        _check_sts(libc.sigemptyset(self._mask_addr))
        return \
            self
    #end empty

    def fill(self) :
        "adds all signals to the mask."
        _check_sts(libc.sigfillset(self._mask_addr))
        return \
            self
    #end fill

    def add(self, sig : SIG) :
        "adds a single signal to the mask. No-op if it’s already there."
        _check_sts(libc.sigaddset(self._mask_addr, SIG(sig).value))
        return \
            self
    #end add

    def remove(self, sig : SIG) :
        "removes a single signal from the mask. No-op if it’s not there."
        _check_sts(libc.sigdelset(self._mask_addr, SIG(sig).value))
        return \
            self
    #end remove

    def __contains__(self, sig : SIG) :
        "is the signal in the mask."
        return \
            libc.sigismember(self._mask_addr, SIG(sig).value) != 0
    #end __contains__

    @property
    def all_set(self) :
        "returns set of SIG items representing all the signals that are set."
        return \
            set(s for s in SIG if s in self)
    #end all_set

    def copy(self) :
        "returns a new SigProcMask initialized to the same signal set" \
        " as the current state of this one."
        return \
            type(self)(initsigs = self.all_set)
    #end copy

    def apply(self, op : int, return_prev : bool = False) :
        "adds or removes the mask signals to those currently being blocked, or" \
        " replaces the block set altogether, depending on op. return_prev indicates" \
        " whether to return a new SigProcMask representing the previous block state." \
        " Or use the alternative convenience methods apply_block(), apply_unblock() or" \
        " apply_replace() to perform the particular operations."
        if return_prev :
            prev = type(self)()
            c_prev = prev._mask_addr
        else :
            prev = c_prev = None
        #end if
        _check_sts(libc.sigprocmask(op, self._mask_addr, c_prev))
        return \
            prev
    #end apply

    def apply_block(self, return_prev : bool = False) :
        "adds the mask signals to those currently being blocked, optionally" \
        " returning the previous state as a new SigProcMask object."
        return \
            self.apply(SIG_BLOCK, return_prev)
    #end apply_block

    def apply_unblock(self, return_prev : bool = False) :
        "removes the mask signals from those currently being blocked, optionally" \
        " returning the previous state as a new SigProcMask object."
        return \
            self.apply(SIG_UNBLOCK, return_prev)
    #end apply_unblock

    def apply_replace(self, return_prev : bool = False) :
        "sets the mask signals as replacing those currently being blocked," \
        " optionally returning the previous state as a new SigProcMask object."
        return \
            self.apply(SIG_SETMASK, return_prev)
    #end apply_replace

#end SigProcMask

class SignalFile :
    "wrapper around a file descriptor returned by signalfd(2)." \
    " Methods are provided to retrieve signal notifications either" \
    " synchronously or asynchronously."

    __slots__ = ("fd", "_ownfd", "__weakref__") # to forestall typos
    _instances = WeakValueDictionary()

    def __new__(celf, fd, ownfd : bool = True) :
        fd = _get_fileno(fd)
        self = celf._instances.get(fd)
        if self == None :
            self = super().__new__(celf)
            self.fd = fd
            self._ownfd = ownfd
            celf._instances[fd] = self
        else :
            assert self._ownfd == ownfd, "inconsistent setting of ownfd flag"
        #end if
        return \
            self
    #end __new__

    @classmethod
    def create(celf, mask : SigProcMask, flags = 0) :
        "opens a new signalfd which will initially notify signals in" \
        " the specified mask, and returns the SignalFile wrapper object."
        res = libc.signalfd(-1, mask._mask_addr, SFD.encode_mask(flags))
        _check_sts(res)
        return \
            celf(res)
    #end create

    def apply(self, mask : SigProcMask) :
        "installs a new set of signals to be notified via this signalfd."
        # flags seem to be ignored, so I don’t bother with a flags arg
        res = libc.signalfd(self.fd, mask._mask_addr, 0)
        _check_sts(res)
    #end apply

    def fileno(self) :
        assert self.fd != None
        return \
            self.fd
    #end fileno

    def close(self) :
        if self.fd != None :
            os.close(self.fd)
            self.fd = None
        #end if
    #end close

    __del__ = close

    def get_next(self) :
        "reads the next signal notification from the file descriptor. Blocks" \
        " unless NONBLOCK was set, in which case None is returned if no events" \
        " are available."
        buf = SIGNALFD.siginfo()
        try :
            nrbytes = os.readv(self.fd, [buf])
        except BlockingIOError :
            nrbytes = None
        #end try
        if nrbytes == 0 :
            raise EOFError("no more events to read")
        #end if
        if nrbytes == None :
            buf = None
        elif nrbytes != ct.sizeof(buf) :
            raise RuntimeError \
                ("expecting to read %d bytes of signal info, got %d" % (ct.sizeof(buf), nrbytes))
        #end if
        return \
            buf
    #end get_next

    async def get_next_async(self, timeout = None) :
        "waits on the event loop until the timeout (if any) elapses or a signal" \
        " notification is available on the file descriptor. Reads and returns the" \
        " notification is there is one, else returns None."

        ready = None

        def read_ready() :
            if not ready.done() :
                ready.set_result(False)
            #end if
        #end read_ready

        def read_timeout() :
            if not ready.done() :
                ready.set_result(True)
            #end if
        #end read_timeout

    #begin get_next_async
        loop = asyncio.get_running_loop()
        ready = loop.create_future()
        loop.add_reader(self.fd, read_ready)
        if timeout != None :
            timeout_task = loop.call_later(timeout, read_timeout)
        else :
            timeout_task = None
        #end if
        timed_out = await ready
        loop.remove_reader(self.fd)
        if timeout_task != None :
            timeout_task.cancel()
        #end if
        if timed_out :
            buf = None
        else :
            buf = self.get_next()
        #end if
        return \
            buf
    #end get_next_async

#end SignalFile

#+
# Overall
#-

def _atexit() :
    # disable all __del__ methods at process termination to avoid segfaults
    for cls in PidFile, SignalFile :
        delattr(cls, "__del__")
    #end for
#end _atexit
atexit.register(_atexit)
del _atexit
